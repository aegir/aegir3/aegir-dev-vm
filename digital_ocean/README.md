Aegir Development VM - DigitalOcean Integration
===============================================

To run your Vagrant box on a [DigitalOcean droplet](https://github.com/devopsgroup-io/vagrant-digitalocean):

1. Ensure that the [Vagrant-Secret
plugin](https://github.com/tcnksm/vagrant-secret) is installed (`vagrant plugin
install vagrant-secret`).
1. Inside the digital_ocean directory, create a Secret file: `cp vagrant.secret.yaml.example .vagrant/secret.yaml`
1. In your Secret file (`.vagrant/secret.yaml`), provide the following:
  1. `do_ssh_key_name`: Supply the name of your key from the DigitalOcean
dashboard.
  1. `do_api_key`: Supply your API token key from the DigitalOcean dashboard
1. Inside the digital_ocean directory, call `make droplet` to launch/provision your
vagrant environment.
1. After that, vagrant commands (`vagrant ssh`, `vagrant status`, etc. should all Just Work from inside the digital_ocean directory.
1. If you need to rebuild your box, you can call `vagrant rebuild` and the
droplet will conveniently be rebuilt from scratch with the same IP address.

